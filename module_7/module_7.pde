Walker[] others = new Walker[10];
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
//wind (0.1, 0)
PVector wind = new PVector(0.1, 0);

void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < others.length; i++) 
  {
    others[i] = new Walker(); 
    //spaced out equally side-by-side & positioned slightly above the screen
    others[i].position = new PVector( 2 * (Window.windowWidth / 10) * (i - 5), 400);
    //random colors, mass & scaled out according to mass
    others[i].mass = 10 - i;
    others[i].scale = others[i].mass * 10;
    others[i].setColor(random(256), random(256), random(256), 255);
  }
}

void draw()
{
  background(255);
  ocean.render();  
  
  for (int i = 0; i < others.length; i++) 
  {
    //gravity (scaled out according to mass) (0, -0.15f * mass)
    PVector gravity = new PVector (0, -0.15 * others[i].mass); 
    
    others[i].render();
    others[i].update();
    others[i].applyForce(wind);
    others[i].applyForce(gravity);
    others[i].checkEdges();
    
    float c = 0.1f;
    float normal = 1;
    float frictionMagnitude = c * normal;
    PVector friction = others[i].velocity.copy();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    others[i].applyForce(friction);
    
    //drag force when the walkers hit the liquid
    if (ocean.isCollidingWith(others[i]))
    {
      PVector dragForce = ocean.calculateDragForce(others[i]);
      others[i].applyForce(dragForce);
    }
  }
}
