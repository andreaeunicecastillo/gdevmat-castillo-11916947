public class Walker
{
  //random position
  public PVector position = new PVector(random(width), random(height));
  
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  //random scale
  public float scale = random(1, 50);
  
  public float velocityLimit = 10;
  public float r = 255, g = 255, b = 255, a = 255;
  
  public void update()
  {
    PVector mouse = new PVector(mouseX, mouseY);
    //get the direction
    PVector direction = PVector.sub(mouse, position);
    //normalize the direction
    direction.normalize();
    //assign direction to the acceleration
    this.acceleration = direction;
    //multiply it to 0.2
    acceleration.mult(0.2);
    
    this.velocity.add(this.acceleration);
    this.velocity.limit(velocityLimit);
    this.position.add(this.velocity);
  }
  
  public void render()
  {
    noStroke();
    fill(r,g,b,a);
    circle(position.x, position.y, scale);
  }
  
  public void checkEdges()
  {
    if(this.position.x > width)
    {
      this.position.x = 0;
    }
    else if (this.position.x < 0)
    {
      this.position.x = width;
    }
    else if (this.position.y > height)
    {
      this.position.y = 0;
    }
    else if (this.position.y < 0)
    {
      this.position.y = height;
    }
  }
  
  public void setColor(float r, float g, float b, float a)
  {
     this.r = r;
     this.g = g;
     this.b = b;
     this.a = a;
  }
}
