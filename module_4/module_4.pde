Walker[] others = new Walker[100];

void setup()
{
  size(1280, 720, P3D);

  for (int i = 0; i < others.length; i++) 
  {
    others[i] = new Walker(); 
    others[i].setColor(random(256), random(256), random(256), random(256));
  }
}

void draw()
{
  background(80);
  
  for (int i = 0; i < others.length; i++) 
  {
      others[i].update();
      others[i].checkEdges();
      others[i].render();
  }
}
