Walker[] walkers = new Walker[8];

PVector wind = new PVector(0.15,0);

void setup()
{
  size(1280, 720, P3D);
  camera (0,0,Window.eyeZ,0,0,0,0,-1,0);
  reset();
}

void reset()
{
   int posX = 0;
   int posY = 0;
   
   for(int i=0; i < 8; i++)
   {
     posX = Window.left;
     posY =  2 * (Window.windowHeight / 8)  * (i - (8 / 2));
     walkers[i] = new Walker();
     walkers[i].position = new PVector(posX, posY); 
     walkers[i].mass = 8-i;
     walkers[i].scale = walkers[i].mass * 15; 
   }
}

void draw()
{
  background(80);
  
  for (Walker w: walkers)
  {  

    float mew = 0.01; 
    float normal = 1;
    float frictionMagnitude = mew * normal;
    PVector friction = w.velocity.copy(); 
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    w.acceleration = new PVector (0.5, 0 * w.mass);
    w.applyForce(w.acceleration);
    w.applyForce(friction);
    
    if(w.position.x > 0)
    {
        mew = 0.4;
    }
    
    w.update();
    w.render();
    w.checkEdges();

  }
}

void mouseClicked()
{
  reset();
}
