Walker[] others = new Walker[10];
//Wind = (0.15, 0)
PVector wind = new PVector(0.15, 0);
//Gravity = (0, -0.4)
PVector gravity = new PVector(0, -0.4);

void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < others.length; i++) 
  {
    others[i] = new Walker();
    //Each walker must have mass values from 1 to 10
    others[i].mass = 10 - i;
    //Each walker must have a scale depending on its mass (mass * 15)
    others[i].scale = others[i].mass * 15;
    //Each walker must have random color
    others[i].setColor(random(256), random(256), random(256), 256);
    //All walkers must start at (-500, 200) position
    others[i].position.x = -500;
    others[i].position.y = 200;
  }
}

void draw()
{
  background(80);
  
  for (int i = 0; i < others.length; i++) 
  {
    others[i].render();
    others[i].update();
    //Apply both forces to all the walkers
    others[i].applyForce(wind);
    others[i].applyForce(gravity);
    //Apply Newton's Third Law by making it bounce from the edges of the screen
    others[i].checkEdges();
  }
  
}
