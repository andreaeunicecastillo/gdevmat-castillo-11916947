public class Walker
{
   public PVector position;
   public float scale = 50;
   public float r = 255; 
   public float g = 255; 
   public float b = 255;  
   public float a = 255;
   
   public float gaussianScale = randomGaussian();
   float standardDeviationScale = 10;
   float meanScale = 10;
   float randScale = standardDeviationScale * gaussianScale + meanScale;
 
   Walker()
   {
      position = new PVector(); 
   }
   
   Walker(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   Walker(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
   Walker(PVector position)
   {
      this.position = position; 
   }
   Walker(PVector position, float scale)
   {
      this.position = position; 
      this.scale = scale;
   }
   
   
   public void render()
   {
     noStroke();
     fill(r,g,b,a);
     circle(position.x, position.y, randScale); 
   }
   
   public void renderBlackHole()
   {
      noStroke();
      fill(r,g,b,a);
      circle(position.x, position.y, scale); 
   }
      
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
} 
