Walker target;
Walker[] other;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  smooth();
  
  other = new Walker[100];
    other[0] = new Walker();
    other[1] = new Walker();
    other[2] = new Walker();
    other[3] = new Walker();
    other[4] = new Walker();
    other[5] = new Walker();
    other[6] = new Walker();
    other[7] = new Walker();  
    other[8] = new Walker();
    other[9] = new Walker();  
    other[10] = new Walker();
    other[11] = new Walker();
    other[12] = new Walker();
    other[13] = new Walker();
    other[14] = new Walker();
    other[15] = new Walker();
    other[16] = new Walker();
    other[17] = new Walker();  
    other[18] = new Walker();
    other[19] = new Walker();  
    other[20] = new Walker();
    other[21] = new Walker();
    other[22] = new Walker();
    other[23] = new Walker();
    other[24] = new Walker();
    other[25] = new Walker();
    other[26] = new Walker();
    other[27] = new Walker();  
    other[28] = new Walker();
    other[29] = new Walker();  
    other[30] = new Walker();
    other[31] = new Walker();
    other[32] = new Walker();
    other[33] = new Walker();
    other[34] = new Walker();
    other[35] = new Walker();
    other[36] = new Walker();
    other[37] = new Walker();  
    other[38] = new Walker();
    other[39] = new Walker();  
    other[40] = new Walker();
    other[41] = new Walker();
    other[42] = new Walker();
    other[43] = new Walker();
    other[44] = new Walker();
    other[45] = new Walker();
    other[46] = new Walker();
    other[47] = new Walker();  
    other[48] = new Walker();
    other[49] = new Walker();  
    other[50] = new Walker();
    other[51] = new Walker();
    other[52] = new Walker();
    other[53] = new Walker();
    other[54] = new Walker();
    other[55] = new Walker();
    other[56] = new Walker();
    other[57] = new Walker();  
    other[58] = new Walker();
    other[59] = new Walker();  
    other[60] = new Walker();
    other[61] = new Walker();
    other[62] = new Walker();
    other[63] = new Walker();
    other[64] = new Walker();
    other[65] = new Walker();
    other[66] = new Walker();
    other[67] = new Walker();  
    other[68] = new Walker();
    other[69] = new Walker();  
    other[70] = new Walker();
    other[71] = new Walker();
    other[72] = new Walker();
    other[73] = new Walker();
    other[74] = new Walker();
    other[75] = new Walker();
    other[76] = new Walker();
    other[77] = new Walker();  
    other[78] = new Walker();
    other[79] = new Walker();  
    other[80] = new Walker();
    other[81] = new Walker();
    other[82] = new Walker();
    other[83] = new Walker();
    other[84] = new Walker();
    other[85] = new Walker();
    other[86] = new Walker();
    other[87] = new Walker();  
    other[88] = new Walker();
    other[89] = new Walker();  
    other[90] = new Walker();
    other[91] = new Walker();
    other[92] = new Walker();
    other[93] = new Walker();
    other[94] = new Walker();
    other[95] = new Walker();
    other[96] = new Walker();
    other[97] = new Walker();  
    other[98] = new Walker();
    other[99] = new Walker();
  target = new Walker();
  
  frameRate(300);
  
  setRandomPositions(target);
  setRandomPositions(other);

}

void setRandomPositions(Walker[] walker)
{
   for(int i = 0; i < other.length; i++)
  {
    other[i].position = new PVector(random(-500, 500),random(-300, 300));
    other[i].setColor(random(256), random(256), random(256), random(256));
  }
}

void setRandomPositions(Walker walker)
{
  target.position = new PVector(random(-500, 500),random(-300, 300));
  target.setColor(255, 255, 255, 255);
}

void draw()
{  
   background(0);
    for(int i = 0; i < other.length; i++)
   {
    other[i].render();
    PVector direction = PVector.sub(target.position, other[i].position);
    other[i].position.add(direction.normalize()).mult(1);
   }
    target.renderBlackHole();
   if (frameCount % 300 == 0)
   {
     setup();
   }
}

 //<>//

    
