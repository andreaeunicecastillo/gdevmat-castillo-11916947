//Create 10 walkers
Walker[] others = new Walker[10];

void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
    
  for (int i = 0; i < others.length; i++) 
  {
    others[i] = new Walker(); 
    //random initial position
    others[i].position = new PVector (random(-500, 500), random(-300,300));
    //random colors, mass, scale(accdg to mass)
    others[i].mass = 10 -i;
    others[i].scale = others[i].mass * 10;
    others[i].setColor(random(256), random(256), random(256), 255);
  }
}

void draw()
{
  background(80);
  
  //They must all be gravitationally attracted to each other
  for (int i = 0; i < 5; i++) 
  {
    others[i].update();
    others[i].render();
    
    for (int j = 5; j < 10; j++) 
    {
      others[j].update();
      others[j].render();
      
      if(i != j)
      {
          others[i].applyForce(others[j].calculateAttraction(others[i]));
          others[j].applyForce(others[i].calculateAttraction(others[j]));
      }
    }
  }
}
