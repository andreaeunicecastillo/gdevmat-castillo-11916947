void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x,y);
}


void draw()
{
  background(130);
  
  PVector mouse = mousePos();
  
  //Red Outer Glow
  strokeWeight(10);
  stroke(255, 0, 0);
  mouse.normalize().mult(300);
  line(0, 0, mouse.x, mouse.y);
  mouse.normalize().mult(-300);
  line(0, 0, mouse.x, mouse.y);

  //White Inner Glow
  strokeWeight(3);
  stroke(255, 255, 255);
  mouse.normalize().mult(300);
  line(0, 0, mouse.x, mouse.y);
  mouse.normalize().mult(-300);
  line(0, 0, mouse.x, mouse.y);
  
  //Black Handle
  strokeWeight(10);
  stroke(0, 0, 0);
  mouse.normalize().mult(80);
  line(0, 0, mouse.x, mouse.y);
  mouse.normalize().mult(-80);
  line(0, 0, mouse.x, mouse.y);
}
