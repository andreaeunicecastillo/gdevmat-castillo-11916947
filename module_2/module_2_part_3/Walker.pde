public class Walker
{
  public float x;
  public float y;
  public float scale;
  public float r;
  public float g;
  public float b;
  public float tx = 0, ty = 10000, tscale = 10, tr = 2554, tg = 2552, tb = 25500;
  
  
  void render()
  {  
    fill (r, g, b);
    noStroke();
    circle(x, y, scale);
  }
  
  void perlinWalk()
  {
    x = map(noise(tx), 0, 1, -640, 640);
    y = map(noise(ty), 0, 1, -360, 360);
    scale = map(noise(tscale), 0, 1, 5, 150);
    
    tx += 0.01f;
    ty += 0.01f;
    tscale += 0.01f;
  }
  
  void perlinColor()
  {
    r = map(noise(tr), 0, 1, 0, 255);
    g = map(noise(tg), 0, 1, 0, 255);
    b = map(noise(tb), 0, 1, 0, 255);
    
    tr += 0.05f;
    tg += 0.05f;
    tb += 0.05f;
  }
}
