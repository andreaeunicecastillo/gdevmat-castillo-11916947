void setup()
{
  size(1020, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0/ 180.0), 0, 0, 0, 0, -1, 0);
}

void draw()
{
  float gaussianX = randomGaussian();
  float standardDeviationX = 100;
  float meanX = 0;
  
  float x = standardDeviationX * gaussianX + meanX;
  
  float gaussianScale = randomGaussian();
  float standardDeviationScale = 30;
  float meanScale = 10;
  
  float scale = standardDeviationScale * gaussianScale + meanScale;
  
  noStroke();
  fill(int(random(256)),int(random(256)),int(random(256)),int(random(10, 101)));
  circle (x, random(-360, 361), scale);
}
